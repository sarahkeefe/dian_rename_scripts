#!/bin/sh
#
# PUP RENAME FOR DIAN
# Sarah Keefe
#
# Last Updated: 5/21/2019
#
# For each row in the given CSV, this script downloads a zipped PUP file set, unzips it, 
# and goes through each file and renames files and file contents for DIAN migration. 
#
# usage: ./pup_rename_dian.sh -z -d -u username:password -f filename.csv
#
# -z --zip to zip the resulting anonymized PUP folder
# -d --download to download from CNDA
# -u --username username:password is a username:password token from CNDA. Can be a 
#     real username and password in the format username:password, or alias:secret token 
#     generated from https://cnda.wustl.edu/data/services/tokens/issue.
# -f --file filename.csv is a csv listing PUP timecourse data to be anonymized.
# The csv should have the following columns in this order:
#    mr_accession_num
#    old_session_label
#    old_pup_id
#    old_pup_label
#    old_subject
#    old_project
#    old_fs_id
#    old_mr_id
#    new_session_label
#    new_pup_id
#    new_pup_label
#    new_subject
#    new_project
#    new_fs_id
#    new_mr_id
#
# If you choose to use this script without the -d option, the script expects a PUP folder in 
# a specific structure. The expected folder structure is:
#
# old_pup_session_label
#   -old_pup_id
#       -out
#           -resources
#             -DATA
#                -files
#                   -pet_proc
#                       PUP data files
#            -LOG
#               -files
#                   -LOGS
#                       log files
#            -SNAPSHOTS
#               -files
#                   -SNAPSHOTS
#                       snapshot files
#
#
#
unset module

key=$1
INFILE=0
SITE=https://cnda.wustl.edu

# setup for NRG environment
#source /nrgpackages/scripts/freesurfer53-patch_setup.sh
#MATLAB_LOCATION=/nrgpackages/tools.release/MATLAB/R2015a/bin

# Setup for linux5.neuroimage.wustl.edu
export FREESURFER_HOME=/data/nil-bluearc/benzinger2/FreeSurfer-5.3-HCP
source $FREESURFER_HOME/SetUpFreeSurfer.sh
MATLAB_LOCATION=/usr/local/bin

#new_pup_datestamp=19700101000000

# Authenticates credentials against XNAT and returns the cookie jar file name. USERNAME and
# PASSWORD must be set before calling this function.
#   USERNAME="foo"
#   PASSWORD="bar"
#   COOKIE_JAR=$(startSession)
startSession() {
    # Authentication to XNAT and store cookies in cookie jar file
    local COOKIE_JAR=.cookies-$(date +%Y%M%d%s).txt
    curl -k -s -u ${USERNAME}:${PASSWORD} --cookie-jar ${COOKIE_JAR} "${SITE}/data/JSESSION" > /dev/null
    echo ${COOKIE_JAR}
}

# Downloads a resource from a URL and stores the results to the specified path. The first parameter
# should be the destination path and the second parameter should be the URL.
download() {
    local OUTPUT=${1}
    local URL=${2}
    curl -H 'Expect:' --keepalive-time 2 -k --cookie ${COOKIE_JAR} -o ${OUTPUT} ${URL}
}

# Ends the user session.
endSession() {
    # Delete the JSESSION token - "log out"
    curl -i -k --cookie ${COOKIE_JAR} -X DELETE "${SITE}/data/JSESSION"
    rm -f ${COOKIE_JAR}
}

# Anonymize MGZ files
anonymizeMgz() {
    local MGZ_FILEPATH=${1}

    echo "MGZ_FILEPATH is ${MGZ_FILEPATH}"

    #MGZ_FILEPATH_BASE=${MGZ_FILEPATH::-4}
    MGZ_FILEPATH_BASE=`echo "${MGZ_FILEPATH}" | rev | cut -c5- | rev`

    echo "MGZ_FILEPATH_BASE is ${MGZ_FILEPATH_BASE}"

    # Use Freesurfer's mri_convert to convert MGZ to MGH
    mri_convert ${MGZ_FILEPATH_BASE}.mgz ${MGZ_FILEPATH_BASE}.mgh

    # Open the file in matlab using load_mgh and save_mgh - this strips the tags
    ${MATLAB_LOCATION}/matlab -nosplash -nodisplay -nojvm -r "[vol,M,mr] = load_mgh('${MGZ_FILEPATH_BASE}.mgh');save_mgh(vol,'${MGZ_FILEPATH_BASE}.stripped.mgh',M,mr);quit()"

    # convert stripped file using mri_convert - don't resize, and reslice like the original mgh
    mri_convert -ns 1 -rl ${MGZ_FILEPATH_BASE}.mgh ${MGZ_FILEPATH_BASE}.stripped.mgh ${MGZ_FILEPATH_BASE}.stripped.mgh

    # convert result back to .mgz format
    mri_convert ${MGZ_FILEPATH_BASE}.stripped.mgh ${MGZ_FILEPATH_BASE}.mgz

    # remove intermediary files
    rm -f ${MGZ_FILEPATH_BASE}.mgh
    rm -f ${MGZ_FILEPATH_BASE}.stripped.mgh
}

# usage instructions
if [ ${#@} == 0 ]; then
    echo ""
    echo "PUP Anonymization Script"
    echo ""
    echo "For each row in the given CSV, this script downloads a zipped PUP file set, "
    echo "    unzips it, and goes through each file and removes lines that may contain "
    echo "    identifying information. It also removes certain files to reduce the final "
    echo "    file size. "
    echo ""   
    echo "Usage: $0 -f filename.csv -r /path/to/save/to -z -d -u username:password"
    echo "    -r --rootdir: path to the folder where you want your anonymized PUP zip files to be stored"    
    echo "    -z --zip to zip the resulting anonymized PUP folder"
    echo "    -d --download to download from CNDA"
    echo "    -u --username username:password is a username:password token from CNDA. Can be "
    echo "        a real username and password in the format username:password, or alias:secret"
    echo "        token generated from https://cnda.wustl.edu/data/services/tokens/issue."
    echo "    -f --file filename.csv is a csv listing PUP timecourse data to be anonymized."
    echo "        The csv should have the following columns in this order:"
    echo "            pet_accession_num"
    echo "            old_session_label"
    echo "            old_pup_id"
    echo "            old_pup_label"
    echo "            old_subject"
    echo "            old_project"
    echo "            old_fs_id"
    echo "            old_mr_id"
    echo "            new_session_label"
    echo "            new_pup_id"
    echo "            new_pup_label"
    echo "            new_subject"
    echo "            new_project"
    echo "            new_fs_id"
    echo "            new_mr_id"
    echo " If you choose to use this script without the -d option, the script expects a "
    echo "    PUP folder in a specific structure. The expected folder structure is: "
    echo "        old_pup_session_label"
    echo "            -old_pup_id"
    echo "                -out"
    echo "                    -resources"
    echo "                      -DATA"
    echo "                         -files"
    echo "                            -pet_proc"
    echo "                                PUP data files"
    echo "                     -LOG"
    echo "                        -files"
    echo "                            -LOGS"
    echo "                                log files"
    echo "                     -SNAPSHOTS"
    echo "                        -files"
    echo "                            -SNAPSHOTS"
    echo "                                snapshot files"
else 

    echo "Script started at $(date +%H:%M:%S)"


    # check arguments
    while [[ $# -gt 0 ]]; do
        key="$1"
        case "$key" in
            -z|--zip)
            ZIP=1
            ;;
            -d|--download)
            DOWNLOAD=1
            ;;
            -f|--file)
            shift #get the value for -f
            INFILE="$1"
            ;;
            -u|--username)
            shift #get the value for -u
            USERNAME="$1"
            ;;
            -r|--rootdir)
            shift #get the value for -r
            BASE_WORK_DIR="$1"
            ;;  
            *)
            echo "unknown option '$key'"
            ;;
        esac
        shift
    done

    # check that we have an $INFILE and end the script if we don't 
    if [[ -z $INFILE ]] ; then
        echo 'No input file entered.'
        exit 1
    fi

    # Read in password
    read -s -p "Enter your password for accessing data on ${SITE}:" PASSWORD

    echo ""

    COOKIE_JAR=$(startSession)   

    LOG_DIR=${BASE_WORK_DIR}/logs
    mkdir -p ${LOG_DIR}

    # Set up log files
    TIMESTAMP=`date +%Y%m%d%H%M%S`
    PROBLEM_LOGFILE=${LOG_DIR}/fs_run_issues_${TIMESTAMP}.csv
    touch ${PROBLEM_LOGFILE}

    cat $INFILE | while IFS=, read -r pet_accession_num old_pup_session_label old_pup_id old_pup_label old_subject old_project old_fs_id old_mr_id new_pup_session_label new_pup_id new_pup_label new_subject new_project new_fs_id new_mr_id; do

        if [ "$pet_accession_num" = "pet_accession_num" ]; then
            echo ""
        else

            PUP_WORKDIR=${BASE_WORK_DIR}/${new_pup_id}

            if [ ! -d ${PUP_WORKDIR} ]; then
               mkdir -p ${PUP_WORKDIR}
               echo PUP_WORKDIR $PUP_WORKDIR
            fi

            if [ "$DOWNLOAD" -eq "1" ]; then

                PUP_DOWNLOAD_URL=${SITE}/data/archive/experiments/${pet_accession_num}/assessors/${old_pup_id}/files?format=zip
                download ${PUP_WORKDIR}/${old_pup_session_label}.zip ${PUP_DOWNLOAD_URL}

                # Test the zip file to see if we got a valid download. 
                # If so, continue the script. 
                if zip -Tq ${PUP_WORKDIR}/${old_pup_session_label}.zip > /dev/null; then

                    unzip ${PUP_WORKDIR}/${old_pup_session_label}.zip -d ${PUP_WORKDIR}
                    rm -f ${PUP_WORKDIR}/${old_pup_session_label}.zip

                    # make directories for data and snapshots
                    mkdir -p ${PUP_WORKDIR}/DATA
                    mkdir -p ${PUP_WORKDIR}/SNAPSHOTS

                    # Move the PUP DATA folder to the main working directory
                    mv ${PUP_WORKDIR}/${old_pup_label}/out/resources/DATA/files/pet_proc ${PUP_WORKDIR}/DATA/

                    # Move the PUP SNAPSHOTS folder to the main working directory
                    mv ${PUP_WORKDIR}/${old_pup_label}/out/resources/SNAPSHOTS/files/SNAPSHOTS ${PUP_WORKDIR}/SNAPSHOTS/

                    # Remove the rest of the XNAT downloaded file structure
                    rm -rf ${PUP_WORKDIR}/${old_pup_label}

                else
                    # There was a problem downloading the Freesurfer zip file
                    # Log it
                    echo "There was a problem downloading PUP ID ${old_pup_id}. Adding the ID to the failed lists."
                    echo "${OLD_FS_ID},${NEW_FS_ID},${MR_ACCESSION_NUM},Unable to download original Freesurfer" >> ${PROBLEM_LOGFILE}        

                fi

            fi

            #echo "ID ${old_pup_id}: Renaming folders."
            #echo "ID ${old_pup_id}: Renaming folders." >> $LOGFILE 2>&1

            # rename $old_pup_label folder to $new_pup_label
            #mv ${PUP_WORKDIR}/$old_pup_label ${PUP_WORKDIR}/$new_pup_label

            #echo "ID ${old_pup_id}: Renaming files in snapshots and pet_proc folders."
            #echo "ID ${old_pup_id}: Renaming files in snapshots and pet_proc folders." >> $LOGFILE 2>&1

            # rename files in SNAPSHOTS/snapshots to replace $old_pup_session_label with $new_pup_session_label
            find ${PUP_WORKDIR}/SNAPSHOTS/SNAPSHOTS -name '*' -type f -exec rename $old_pup_session_label $new_pup_session_label '*' {} \;

            # rename files in DATA/ to replace $old_pup_session_label with $new_pup_session_label
            # run this twice to take care of duplicates - some files have ${old_pup_session_label}_XYZ_${old_pup_session_label}
            find ${PUP_WORKDIR}/DATA/pet_proc -name '*' -type f -exec rename $old_pup_session_label $new_pup_session_label '*' {} \;
            find ${PUP_WORKDIR}/DATA/pet_proc -name '*' -type f -exec rename $old_pup_session_label $new_pup_session_label '*' {} \;

            #echo "ID ${old_pup_id}: Deleting log files in pet_proc."
            #echo "ID ${old_pup_id}: Deleting log files in pet_proc." >> $LOGFILE 2>&1

            # remove log files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name '*.log' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name '*.log.bak' -type f -exec rm -rf {} \;

            #echo "ID ${old_pup_id}: Deleting .rec files in pet_proc."
            #echo "ID ${old_pup_id}: Deleting .rec files in pet_proc." >> $LOGFILE 2>&1

            # remove rec files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name '*.rec' -type f -exec rm -rf {} \;


            #echo "ID ${old_pup_id}: Deleting unnecessary 4dfp files."
            #echo "ID ${old_pup_id}: Deleting unnecessary 4dfp files." >> $LOGFILE 2>&1

            # remove BrainMask_b80.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'BrainMask_b80.4dfp.img' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'BrainMask_b80.4dfp.hdr' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'BrainMask_b80.4dfp.ifh' -type f -exec rm -rf {} \;

            # remove T1001_b80.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_b80.4dfp.img' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_b80.4dfp.hdr' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_b80.4dfp.ifh' -type f -exec rm -rf {} \;

            # remove T1001_g9.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_g9.4dfp.img' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_g9.4dfp.hdr' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'T1001_g9.4dfp.ifh' -type f -exec rm -rf {} \;

            # remove ${new_pup_session_label}n_tb*.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_tb*.4dfp.img -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_tb*.4dfp.hdr -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_tb*.4dfp.ifh -type f -exec rm -rf {} \;

            # remove ${new_pup_session_label}n_msum*.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_msum*.4dfp.img -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_msum*.4dfp.hdr -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_msum*.4dfp.ifh -type f -exec rm -rf {} \;

            # remove ${new_pup_session_label}n_sumall*.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_sumall*.4dfp.img -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_sumall*.4dfp.hdr -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name ${new_pup_session_label}n_sumall*.4dfp.ifh -type f -exec rm -rf {} \;

            # remove RSFMask.4dfp.* files
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'RSFMask.4dfp.img' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'RSFMask.4dfp.hdr' -type f -exec rm -rf {} \;
            #find ${PUP_WORKDIR}/DATA/pet_proc -name 'RSFMask.4dfp.ifh' -type f -exec rm -rf {} \;


            #echo "ID ${old_pup_id}: Deleting unnecessary .tac files."
            #echo "ID ${old_pup_id}: Deleting unnecessary .tac files." >> $LOGFILE 2>&1

            # remove files ending in _PVC2C.tac
            #find ${PUP_WORKDIR}/DATA/pet_proc -name *_PVC2C.tac -type f -exec rm -rf {} \;


            # Modify the old_pup_session_label.params file
            # 

            #echo "ID ${old_pup_id}: Anonymizing .params file."
            #echo "ID ${old_pup_id}: Anonymizing .params file." >> $LOGFILE 2>&1

            # remove lines starting with "userfullname"
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)        
            #find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i '/^userfullname/d' {} ';'

            # remove lines starting with "useremail"
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)        
            #find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i '/^useremail/d' {} ';'

            # remove lines starting with "datestamp"
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)        
            #find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i '/^datestamp/d' {} ';'

            # replace oldsubjectid with newsubjectid
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_subject}'/'${new_subject}'/g' {} ';'

            # replace oldproject with newproject
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_project}'/'${new_project}'/g' {} ';'

            # replace old_fs_id with new_fs_id
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)    
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_fs_id}'/'${new_fs_id}'/g' {} ';'

            # replace old_pup_id with new_pup_id
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)     
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_pup_id}'/'${new_pup_id}'/g' {} ';'

            # replace =$old_pup_label with =$new_pup_label
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params) 
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_pup_label}'/'${new_pup_label}'/g' {} ';'

            # replace mr_accession_num with newsubjectid
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${pet_accession_num}'/'${new_pup_session_label}'/g' {} ';'

            # replace pup datestamp
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)      
            #find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_pup_datestamp}'_[0-9]\{6\}/'${new_pup_datestamp}'/g' {} ';'

            # replace old_mr_id with new_mr_id
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)
            # do this at the end since the MR ID is also part of the Freesurfer ID      
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_mr_id}'/'${new_mr_id}'/g' {} ';'

            # replace =$old_pup_session_label with =$new_pup_session_label
            # do this only for old_pup_session_label.params (which has been renamed to ${new_pup_session_label}.params)
            # do this last since this string is contained in other strings
            find ${PUP_WORKDIR}/DATA/pet_proc/${new_pup_session_label}.params -type f -exec sed -i -e 's/'${old_pup_session_label}'/'${new_pup_session_label}'/g' {} ';'


            # Modify all files

            #echo "ID ${old_pup_id}: Replacing pup session label in pet_proc folder."
            #echo "ID ${old_pup_id}: Replacing pup session label in pet_proc folder." >> $LOGFILE 2>&1

            # replace $old_pup_session_label with $old_pup_session_label
            # do this for ALL files
            find ${PUP_WORKDIR}/DATA/pet_proc -type f -exec sed -i -e 's/'${old_pup_session_label}'/'${new_pup_session_label}'/g' {} ';'

            #echo "ID ${old_pup_id}: Replacing date in .hdr files."
            #echo "ID ${old_pup_id}: Replacing date in .hdr files." >> $LOGFILE 2>&1

            # replace existing PUP dates with null date in .hdr files
            # do this only for .hdr files
            #find ${PUP_WORKDIR}/DATA/pet_proc -type f -name '*.hdr' -exec sed -i 's/[A-Z]\{1\}[a-z]\{2\}[A-Z]\{1\}[a-z]\{2\}[0-9]\{2\}/ThuJan01/g' {} +
            #find ${PUP_WORKDIR}/DATA/pet_proc -type f -name '*.hdr' -exec sed -i 's/[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}/00:00:00/g' {} +


            ####
            ## Remove T1.mgz and wmparc.mgz ##
            #echo "ID ${old_pup_id}: Deleting T1.mgz and wmparc.mgz."
            #echo "ID ${old_pup_id}: Deleting T1.mgz and wmparc.mgz." >> $LOGFILE 2>&1            
            #rm -f ${PUP_WORKDIR}/DATA/pet_proc/T1.mgz
            #rm -f ${PUP_WORKDIR}/DATA/pet_proc/wmparc.mgz
            ####

            # Anonymize T1.mgz and wmparc.mgz
            for MGZ_FILEPATH in ${PUP_WORKDIR}/DATA/pet_proc/*.mgz; do
                if [ -s ${MGZ_FILEPATH} ]; then
                    anonymizeMgz $MGZ_FILEPATH
                fi
            done

            #echo "ID ${old_pup_id}: All done with anonymization!"
            #echo "ID ${old_pup_id}: All done with anonymization!" >> $LOGFILE 2>&1

            if [ "$ZIP" -eq "1" ]; then

                mainDir=`pwd`

                zipDir=${PUP_WORKDIR}/DATA
                cd ${zipDir}

                # zip the result up               
                zip -r ${new_pup_label}.zip pet_proc
                mv ${new_pup_label}.zip ../${new_pup_label}.zip

                cd $mainDir

                snapsDir=${PUP_WORKDIR}/SNAPSHOTS
                cd ${snapsDir}

                # zip the snaps up               
                zip -r ${new_pup_label}_snapshots.zip SNAPSHOTS
                mv ${new_pup_label}_snapshots.zip ../${new_pup_label}_snapshots.zip            

                cd $mainDir

            fi

        fi

    done < $INFILE

fi