#!/bin/bash
#
# FREESURFER RENAME FOR DIAN
#
# Last Updated: 5/20/2019
#
# This script goes through each file in each Freesurfer folder in the given csv and 
# removes lines that may contain identifying information. It uses 
# Freesurfer's mri_convert and Matlab load_mgh() and save_mgh()
# functions to strip tags from the ends of .mgz filetypes.
# This script preserves run dates and times.
#
# To run this script you must have Freesurfer set up locally:
# https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall#Setup.26Configuration
#
# You must also have Matlab set up for the command line.
# https://www.mathworks.com/help/install/installation-and-activation-for-users.html
# https://www.mathworks.com/help/matlab/ref/matlablinux.html
#
# usage: ./fs_anonymization_v3.sh -d -u username:password -z -f filename.csv
#
# -d to download the file from CNDA (no need for this if the folder is stored locally)
# -u for CNDA username and password (also accepts alias:secret issued from
# https://cnda.wustl.edu/data/services/tokens/issue)
# -z to zip the resulting anonymized FS folder
# -r to set the path to the folder where you want the anonymized zipped freesurfer to be stored
# filename.csv is a csv listing existing FS folders in the current directory.
# The csv should have the following columns in this order:
#
#
# MR_ACCESSION_NUM
# OLD_SESSION_LABEL
# OLD_SUBJECT_ID
# OLD_PROJECT
# OLD_FS_ID
# NEW_SESSION_LABEL
# NEW_SUBJECT_ID
# NEW_PROJECT
# NEW_FS_ID
#
#
# The expected folder structure is:
# - ROOT_DIR
#      - OLD_FS_ID
#          -OLD_SESSION_LABEL
#                label
#                mri
#                scripts
#                stats
#                surf
#                touch
#
#
#
unset module 

# setup for NRG environment
#source /nrgpackages/scripts/freesurfer53-patch_setup.sh
#MATLAB_LOCATION=/nrgpackages/tools.release/MATLAB/R2015a/bin

# Setup for linux5.neuroimage.wustl.edu
export FREESURFER_HOME=/data/nil-bluearc/benzinger2/FreeSurfer-5.3-HCP
source $FREESURFER_HOME/SetUpFreeSurfer.sh
MATLAB_LOCATION=/usr/local/bin

key=$1
INFILE=0
SITE=https://cnda.wustl.edu


# Authenticates credentials against XNAT and returns the cookie jar file name. USERNAME and
# PASSWORD must be set before calling this function.
#   USERNAME="foo"
#   PASSWORD="bar"
#   COOKIE_JAR=$(startSession)
startSession() {
    # Authentication to XNAT and store cookies in cookie jar file
    local COOKIE_JAR=.cookies-$(date +%Y%M%d%s).txt
    curl -k -s -u ${USERNAME}:${PASSWORD} --cookie-jar ${COOKIE_JAR} "${SITE}/data/JSESSION" > /dev/null
    echo ${COOKIE_JAR}
}

# Downloads a resource from a URL and stores the results to the specified path. The first parameter
# should be the destination path and the second parameter should be the URL.
download() {
    local OUTPUT=${1}
    local URL=${2}
    curl -H 'Expect:' --keepalive-time 2 -k --cookie ${COOKIE_JAR} -o ${OUTPUT} ${URL}
}

# Ends the user session.
endSession() {
    # Delete the JSESSION token - "log out"
    curl -i -k --cookie ${COOKIE_JAR} -X DELETE "${SITE}/data/JSESSION"
    rm -f ${COOKIE_JAR}
}

# Anonymize MGZ files
anonymizeMgz() {
    local MGZ_FILEPATH=${1}

    echo "MGZ_FILEPATH is ${MGZ_FILEPATH}"

    #MGZ_FILEPATH_BASE=${MGZ_FILEPATH::-4}
    MGZ_FILEPATH_BASE=`echo "${MGZ_FILEPATH}" | rev | cut -c5- | rev`

    echo "MGZ_FILEPATH_BASE is ${MGZ_FILEPATH_BASE}"

    # Use Freesurfer's mri_convert to convert MGZ to MGH
    mri_convert ${MGZ_FILEPATH_BASE}.mgz ${MGZ_FILEPATH_BASE}.mgh

    # Open the file in matlab using load_mgh and save_mgh - this strips the tags
    ${MATLAB_LOCATION}/matlab -nosplash -nodisplay -nojvm -r "[vol,M,mr] = load_mgh('${MGZ_FILEPATH_BASE}.mgh');save_mgh(vol,'${MGZ_FILEPATH_BASE}.stripped.mgh',M,mr);quit()"

    # convert stripped file using mri_convert - don't resize, and reslice like the original mgh
    mri_convert -ns 1 -rl ${MGZ_FILEPATH_BASE}.mgh ${MGZ_FILEPATH_BASE}.stripped.mgh ${MGZ_FILEPATH_BASE}.stripped.mgh

    # convert result back to .mgz format
    mri_convert ${MGZ_FILEPATH_BASE}.stripped.mgh ${MGZ_FILEPATH_BASE}.mgz

    # remove intermediary files
    rm -f ${MGZ_FILEPATH_BASE}.mgh
    rm -f ${MGZ_FILEPATH_BASE}.stripped.mgh
}

# usage instructions
if [ ${#@} == 0 ]; then
    echo ""
    echo "Freesurfer Anonymization Script"
    echo ""
    echo "This script goes through each file in each Freesurfer folder in the given csv and "
    echo "removes lines that may contain identifying information. It also"
    echo "uses Freesurfer's mri_convert and Matlab load_mgh() and save_mgh()"
    echo "functions to strip tags from the ends of .mgz filetypes."
    echo ""
    echo "To run this script you must have Freesurfer set up locally:"
    echo "https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall#Setup.26Configuration"
    echo "You must also have Matlab set up for the command line."
    echo "https://www.mathworks.com/help/install/installation-and-activation-for-users.html"
    echo "https://www.mathworks.com/help/matlab/ref/matlablinux.html"
    echo ""     
    echo "Usage: $0 filename.csv BASE_WORK_DIR -z -d -u username"
    echo "* -f filename.csv: A CSV file containing the following columns (with a header row):"
    echo "     MR_ACCESSION_NUM"
    echo "     OLD_SESSION_LABEL"
    echo "     OLD_SUBJECT_ID"
    echo "     OLD_PROJECT"
    echo "     OLD_FS_ID"
    echo "     NEW_SESSION_LABEL"
    echo "     NEW_SUBJECT_ID"
    echo "     NEW_PROJECT"
    echo "     NEW_FS_ID"
    echo "* -r --rootdir: path to the folder where you want your anonymized Freesurfer zip files to be stored"
    echo "* -z --zip: Zip the file after it is anonymized"
    echo "* -d --download: Download the FS zip folder from CNDA"    
    echo "* -u --username: username for CNDA, used for downloading Freesurfers to anonymize"
else 

    # check arguments
    while [[ $# -gt 0 ]]; do
        key="$1"
        case "$key" in
            -z|--zip)
            ZIP=1
            ;;
            -d|--download)
            DOWNLOAD=1
            ;;
            -f|--file)
            shift #get the value for -f
            INFILE="$1"
            ;;
            -u|--username)
            shift #get the value for -u
            USERNAME="$1"
            ;;
            -r|--rootdir)
            shift #get the value for -r
            BASE_WORK_DIR="$1"
            ;;  
            *)
            echo "unknown option '$key'"
            ;;
        esac
        shift
    done

    LOG_DIR=${BASE_WORK_DIR}/logs
    mkdir -p ${LOG_DIR}

    # Set up log files
    TIMESTAMP=`date +%Y%m%d%H%M%S`
    PROBLEM_LOGFILE=${LOG_DIR}/fs_run_issues_${TIMESTAMP}.csv
    touch ${PROBLEM_LOGFILE}

    # check that we have an $INFILE and end the script if we don't 
    if [[ -z "$INFILE" ]] ; then
        echo 'No input file entered.'
        #echo 'No input file entered.' >> $LOGFILE 2>&1
        exit 1
    fi

    # Read in password
    read -s -p "Enter your password for accessing data on ${SITE} as username ${USERNAME}:" PASSWORD

    echo ""

    COOKIE_JAR=$(startSession)

    echo $COOKIE_JAR

    MAIN_DIR=`pwd`

    cat $INFILE | while IFS=, read -r MR_ACCESSION_NUM OLD_SESSION_LABEL OLD_SUBJECT_ID OLD_PROJECT OLD_FS_ID NEW_SESSION_LABEL NEW_SUBJECT_ID NEW_PROJECT NEW_FS_ID; do

        cd ${MAIN_DIR}

        #echo "export MATLABROOT=/usr/local/MATLAB/R2015a 
        #echo "export MATLAB_JAVA=/usr/lib/jvm/java/jre

        FS_WORKDIR=${BASE_WORK_DIR}/${NEW_FS_ID}
        if [ ! -d ${FS_WORKDIR} ]; then
           mkdir -p ${FS_WORKDIR}
           echo FS_WORKDIR $FS_WORKDIR
           #FS_LOGDIR=${FS_WORKDIR}/logs
           #mkdir ${FS_LOGDIR}
        fi
        export SUBJECTS_DIR=${FS_WORKDIR}

        if [ "$DOWNLOAD" -eq "1" ]; then

            FREESURFER_DOWNLOAD_URL=${SITE}/data/experiments/${MR_ACCESSION_NUM}/assessors/${OLD_FS_ID}/files?format=zip
            echo "$FREESURFER_DOWNLOAD_URL"
            download ${FS_WORKDIR}/${OLD_SESSION_LABEL}.zip ${FREESURFER_DOWNLOAD_URL}

            # Test the zip file to see if we got a valid download. 
            # If so, continue the script. 
            if zip -Tq ${FS_WORKDIR}/${OLD_SESSION_LABEL}.zip > /dev/null; then

                unzip ${FS_WORKDIR}/${OLD_SESSION_LABEL}.zip -d ${FS_WORKDIR}
                rm -f ${FS_WORKDIR}/${OLD_SESSION_LABEL}.zip

                # Move the Freesurfer folder to the main working directory
                mv ${FS_WORKDIR}/${OLD_FS_ID}/out/resources/DATA/files/${OLD_SESSION_LABEL} ${FS_WORKDIR}/${OLD_SESSION_LABEL}

                # Remove the rest of the XNAT downloaded file structure
                rm -rf ${FS_WORKDIR}/${OLD_FS_ID}

            else
                # There was a problem downloading the Freesurfer zip file
                # Log it
                echo "There was a problem downloading Freesurfer ID ${OLD_FS_ID}. Adding the ID to the failed lists."
                echo "${OLD_FS_ID},${NEW_FS_ID},${MR_ACCESSION_NUM},Unable to download original Freesurfer" >> ${PROBLEM_LOGFILE}         

            fi
        fi

        if [ -d ${FS_WORKDIR}/${OLD_SESSION_LABEL} ]; then


            # delete touch folder from DATA
            find ${FS_WORKDIR}/${OLD_SESSION_LABEL} -name 'touch' -type d -prune -exec rm -rf {} \;

            # remove log files
            find ${FS_WORKDIR}/${OLD_SESSION_LABEL} -name '*.log' -type f -exec rm -rf {} \;
            find ${FS_WORKDIR}/${OLD_SESSION_LABEL} -name '*.log.bak' -type f -exec rm -rf {} \;
            find ${FS_WORKDIR}/${OLD_SESSION_LABEL} -name '*.log.old' -type f -exec rm -rf {} \;

            # rename DATA/$OLD_SESSION_LABEL to DATA/$NEW_SESSION_LABEL
            mv ${FS_WORKDIR}/${OLD_SESSION_LABEL} ${FS_WORKDIR}/${NEW_SESSION_LABEL}

            # remove atlas folder if it exists (Freesurfer 5.1)
            if [ -d '${FS_WORKDIR}/${NEW_SESSION_LABEL}/atlas' ]; then              
                find ${FS_WORKDIR}/${NEW_SESSION_LABEL} -name 'atlas' -type d -prune -exec rm -rf {} \;
            fi

            # remove temp folder if it exists
            if [ -d '${FS_WORKDIR}/${NEW_SESSION_LABEL}/temp' ]; then              
                find ${FS_WORKDIR}/${NEW_SESSION_LABEL} -name 'temp' -type d -prune -exec rm -rf {} \;
            fi

            # Replace strings in .batch and .params files that may exist in the main folder
            # replace OLD_FS_ID with NEW_FS_ID
            # do this first since the OLD_SESSION_LABEL frequently is part of the OLD_FS_ID
            find ${FS_WORKDIR}/${NEW_SESSION_LABEL} -type f -exec sed -i 's/'${OLD_FS_ID}'/'${NEW_FS_ID}'/g' {} +

            # replace OLD_SESSION_LABEL with NEW_SESSION_LABEL
            find ${FS_WORKDIR}/${NEW_SESSION_LABEL} -type f -exec sed -i 's/'${OLD_SESSION_LABEL}'/'${NEW_SESSION_LABEL}'/g' {} +

            # replace oldproject with newproject
            find ${FS_WORKDIR}/${NEW_SESSION_LABEL} -type f -exec sed -i 's/'${OLD_PROJECT}'/'${NEW_PROJECT}'/g' {} +



            ####################
            # /LABEL DIRECTORY
            ####################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/label
            #cd ${CURRENT_DIR}

            # remove various lines containing potentially identifying info 
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# written by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# created by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# hostname/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# user/d' {} + 
            # find ${CURRENT_DIR} -type f -exec sed -i '/^filename /d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOST/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^START_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^END_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^USER/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# CreationTime/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# ColorTableTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# InVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# AnnotationFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SegVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SUBJECTS_DIR/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# cmdline/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOSTNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^UNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^JOB_ID/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^Linux/d' {} +

            # replace OLD_FS_ID with NEW_FS_ID
            # do this first since the OLD_SESSION_LABEL frequently is part of the OLD_FS_ID
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_FS_ID}'/'${NEW_FS_ID}'/g' {} +

            # replace /OLD_SESSION_LABEL/ with /NEW_SESSION_LABEL/
            find ${CURRENT_DIR} -type f -exec sed -i 's/\/'${OLD_SESSION_LABEL}'\//\/'${NEW_SESSION_LABEL}'\//g' {} +

            # replace # subjectname OLD_SESSION_LABEL with # subjectname NEW_SESSION_LABEL            
            find ${CURRENT_DIR} -type f -exec sed -i 's/#\ssubjectname\s'${OLD_SESSION_LABEL}'/# subjectname '${NEW_SESSION_LABEL}'/g' {} +

            # replace 'subject OLD_SESSION_LABEL' with 'subject NEW_SESSION_LABEL'           
            find ${CURRENT_DIR} -type f -exec sed -i 's/subject\s'${OLD_SESSION_LABEL}'/subject '${NEW_SESSION_LABEL}'/g' {} +         

            # replace oldproject with newproject
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_PROJECT}'/'${NEW_PROJECT}'/g' {} +


            #################
            # /MRI DIRECTORY
            #################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/mri
            #cd ${CURRENT_DIR}

            # Delete all NIFTI files - we will include these separately for OASIS
            # find ${CURRENT_DIR} -name "*.nii.gz" -type f -exec rm -rf {} \;

            for MGZ_FILEPATH in ${CURRENT_DIR}/*.mgz; do
                if [ -s ${MGZ_FILEPATH} ]; then
                    anonymizeMgz $MGZ_FILEPATH
                fi
            done

            ######################
            # /MRI/ORIG DIRECTORY
            ######################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/mri/orig
            #cd ${CURRENT_DIR}            

            for MGZ_FILEPATH in ${CURRENT_DIR}/*.mgz; do
                if [ -s ${MGZ_FILEPATH} ]; then
                    anonymizeMgz $MGZ_FILEPATH
                fi
            done


            ############################
            # /MRI/TRANSFORMS DIRECTORY
            ############################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/mri/transforms
            #cd ${CURRENT_DIR}

            for MGZ_FILEPATH in ${CURRENT_DIR}/*.mgz; do
                if [ -s ${MGZ_FILEPATH} ]; then
                    anonymizeMgz $MGZ_FILEPATH
                fi
            done

            # remove various lines containing potentially identifying info 
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# written by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# created by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# transform file/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# hostname/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# user/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^filename /d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOST/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^START_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^END_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^USER/d' {} +       
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# CreationTime/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# ColorTableTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# InVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# AnnotationFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SegVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SUBJECTS_DIR/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# cmdline/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOSTNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^UNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^JOB_ID/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^Linux/d' {} +


            # replace OLD_FS_ID with NEW_FS_ID
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_FS_ID}'/'${NEW_FS_ID}'/g' {} +

            # replace /OLD_SESSION_LABEL/ with /NEW_SESSION_LABEL/
            find ${CURRENT_DIR} -type f -exec sed -i 's/\/'${OLD_SESSION_LABEL}'\//\/'${NEW_SESSION_LABEL}'\//g' {} +

            # replace # subjectname OLD_SESSION_LABEL with # subjectname NEW_SESSION_LABEL            
            find ${CURRENT_DIR} -type f -exec sed -i 's/# subjectname '${OLD_SESSION_LABEL}'/# subjectname '${NEW_SESSION_LABEL}'/g' {} +

            # replace 'subject OLD_SESSION_LABEL' with 'subject NEW_SESSION_LABEL'           
            find ${CURRENT_DIR} -type f -exec sed -i 's/subject '${OLD_SESSION_LABEL}'/subject '${NEW_SESSION_LABEL}'/g' {} +

            # replace oldproject with newproject
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_PROJECT}'/'${NEW_PROJECT}'/g' {} +



            #####################
            # /SCRIPTS DIRECTORY
            #####################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/scripts
            #cd ${CURRENT_DIR}

            # delete recon-all.local-copy and recon-all.env
            #rm -f ${CURRENT_DIR}/recon-all.local-copy
            #rm -f ${CURRENT_DIR}/recon-all.env
            rm -f ${CURRENT_DIR}/recon-all.env.bak

            # remove various lines containing potentially identifying info 
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# written by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# created by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# hostname/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# user/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^filename /d' {} +          
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOST/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^START_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^END_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^USER/d' {} +       
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# CreationTime/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# ColorTableTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# InVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# AnnotationFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SegVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SUBJECTS_DIR/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# cmdline/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOSTNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^UNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^JOB_ID/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^Linux/d' {} +

            # replace OLD_FS_ID with NEW_FS_ID
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_FS_ID}'/'${NEW_FS_ID}'/g' {} +

            # replace OLD_SESSION_LABEL with NEW_SESSION_LABEL
            # no values to worry about overwriting in this folder
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_SESSION_LABEL}'/'${NEW_SESSION_LABEL}'/g' {} +      

            # replace oldproject with newproject
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_PROJECT}'/'${NEW_PROJECT}'/g' {} +

            # replace datestamp1_time and datestamp2_time and datestamp3_time
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP1}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP2}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP3}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'

            # replace datestamp1 and datestamp2 and datestamp3
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP1}'/'${NEW_FS_DATESTAMP}'/g' {} ';'
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP2}'/'${NEW_FS_DATESTAMP}'/g' {} ';'
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP3}'/'${NEW_FS_DATESTAMP}'/g' {} ';'

            # replace written out date string
            # matches dates written as Thu Jan 01 00:00:00 CST 1970
            # this is only in recon-all.cmd
            #find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z]{3} [a-zA-Z]{3} *[0-9]* [0-9]{2}:[0-9]{2}:[0-9]{2} [a-zA-Z]{3} [0-9]{4}/Thu Jan 01 00:00:00 CST 1970/g' {} ';'

            # anonymize IMA or DICOM file names
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.IMA/'file.IMA'/g' {} ';'
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.ima/'file.ima'/g' {} ';'
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.DCM/'file.DCM'/g' {} ';'
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.dcm/'file.dcm'/g' {} ';'
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.DICOM/'file.DICOM'/g' {} ';'
            find ${CURRENT_DIR} -type f -exec sed -i -e 's/[a-zA-Z0-9._-]*.dicom/'file.dicom'/g' {} ';'



            ###################
            # /STATS DIRECTORY
            ###################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/stats
            #cd ${CURRENT_DIR}

            # remove various lines containing potentially identifying info 
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# written by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# created by/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# hostname/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# user/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^filename /d' {} +                
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOST/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^START_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^END_TIME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^USER/d' {} +      
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# CreationTime/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# ColorTableTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# InVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# AnnotationFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SegVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# PVVolFileTimeStamp/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# SUBJECTS_DIR/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^# cmdline/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^HOSTNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^UNAME/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^JOB_ID/d' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i '/^Linux/d' {} +

            # replace OLD_FS_ID with NEW_FS_ID
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_FS_ID}'/'${NEW_FS_ID}'/g' {} +

            # replace /OLD_SESSION_LABEL/ with /NEW_SESSION_LABEL/
            find ${CURRENT_DIR} -type f -exec sed -i 's/\/'${OLD_SESSION_LABEL}'\//\/'${NEW_SESSION_LABEL}'\//g' {} +

            # replace # subjectname OLD_SESSION_LABEL with # subjectname NEW_SESSION_LABEL
            find ${CURRENT_DIR} -type f -exec sed -i 's/#\ssubjectname\s'${OLD_SESSION_LABEL}'/# subjectname '${NEW_SESSION_LABEL}'/g' {} +

            # replace 'subject OLD_SESSION_LABEL' with 'subject NEW_SESSION_LABEL'           
            find ${CURRENT_DIR} -type f -exec sed -i 's/subject\s'${OLD_SESSION_LABEL}'/subject '${NEW_SESSION_LABEL}'/g' {} +

            # replace '# Annot OLD_SESSION_LABEL' with # Annot NEW_SESSION_LABEL'           
            find ${CURRENT_DIR} -type f -exec sed -i 's/#\sAnnot\s'${OLD_SESSION_LABEL}'/# Annot '${NEW_SESSION_LABEL}'/g' {} +

            # replace '.OLD_SESSION_LABEL.' with '.NEW_SESSION_LABEL.'           
            find ${CURRENT_DIR} -type f -exec sed -i 's/\.'${OLD_SESSION_LABEL}'\./\.'${NEW_SESSION_LABEL}'\./g' {} +   

            # replace oldproject with newproject
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_PROJECT}'/'${NEW_PROJECT}'/g' {} +

            # replace datestamp1_time and datestamp2_time and datestamp3_time
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP1}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP2}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP3}'_[0-9]{6}/'${NEW_FS_DATESTAMP}_000000'/g' {} ';'

            # replace datestamp1 and datestamp2 and datestamp3
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP1}'/'${NEW_FS_DATESTAMP}'/g' {} ';'
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP2}'/'${NEW_FS_DATESTAMP}'/g' {} ';'
            # find ${CURRENT_DIR} -type f -exec sed -i -e 's/'${OLD_FS_DATESTAMP3}'/'${NEW_FS_DATESTAMP}'/g' {} ';'



            ##################
            # /SURF DIRECTORY
            ##################

            #cd ${MAIN_DIR}

            CURRENT_DIR=${FS_WORKDIR}/${NEW_SESSION_LABEL}/surf
            #cd ${CURRENT_DIR}

            # remove various lines containing potentially identifying info 

            # replace /OLD_SESSION_LABEL/ with /NEW_SESSION_LABEL/
            find ${CURRENT_DIR} -type f -exec sed -i 's/'${OLD_SESSION_LABEL}'/'${NEW_SESSION_LABEL}'/g' {} +

            #find ${CURRENT_DIR} -type f -exec sed -i 's/created by [a-zA-Z0-9.-]*/created by anon/g' {} +
            #find ${CURRENT_DIR} -type f -exec sed -i 's/User: [a-zA-Z0-9.-]*/User: anon/g' {} +
            #find ${CURRENT_DIR} -type f -exec sed -i 's/Machine: [a-zA-Z0-9.-]*/Machine: anon/g' {} +

            # Date replacement - three formats
            # Thu Jan 1 00:00:00 1970
            # find ${CURRENT_DIR} -type f -exec sed -i 's/[a-zA-Z]{3} [a-zA-Z]{3} [0-9]* [0-9]{2}:[0-9]{2}:[0-9]{2} [0-9]{4}/Thu Jan 1 00:00:00 1970/g' {} +
            # find ${CURRENT_DIR} -type f -exec sed -i 's/[a-zA-Z]{3} [a-zA-Z]{3}  [0-9]* [0-9]{2}:[0-9]{2}:[0-9]{2} [0-9]{4}/Thu Jan  1 00:00:00 1970/g' {} +
            # 1970/01/01-00:00:00-GMT
            # find ${CURRENT_DIR} -type f -exec sed -i 's/[0-9]{4}\/[0-9]{2}\/[0-9]{2}-[0-9]{2}:[0-9]{2}:[0-9]{2}-[a-zA-Z]{3}/1970\/01\/01-00:00:00-GMT/g' {} +
            # Jan 1 1970 00:00:00
            # find ${CURRENT_DIR} -type f -exec sed -i 's/[a-zA-Z]* [0-9]* [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}/Jan 1 1970 00:00:00/g' {} +

            echo "Done with rename."

            if [ "$ZIP" -eq "1" ]; then

                cd ${MAIN_DIR}

                echo "Zipping the anonymized Freesurfer."
                cd ${FS_WORKDIR}

                zip -r ${NEW_FS_ID}.zip ${NEW_SESSION_LABEL}

                cd ${MAIN_DIR}

            fi

            echo "Done with one Freesurfer."

        fi

    done
fi